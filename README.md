## <a name="configure"></a>Configure


# This is an H1
## This is an H2
###### This is an H6

This is also an H1
==================

This is also an H2


For example:

| Item | Width | Height | Rate | Bit Depth | Mbps | Packed | Frames @ 70% | % of Aval. Bandwidth |
|-----------------|:-----:|:------:|:----:|:---------:|:-----:|:--------:|:------------:|:--------------------:|
| Overlay Write | 1920 | 1080 | 30 | 30 | 1866 | 1990.656 | 15.0 | 6.66% |
| Overlay Read | 1920 | 1080 | 60 | 30 | 3732 | 3981.312 | 7.5 | 13.32% |
| Watermark Read | 1920 | 1080 | 60 | 30 | 3732 | 3981.312 | 7.5 | 13.32% |
| Watermark Write | 1920 | 1080 | 0 | 30 | 0 | 0 | 0 | 0% |



| Day     | Meal    | Price |
| --------|---------|:-------:|
| Monday  | pasta   | $6    |
| Tuesday | chicken | $8    |


Right     | Left   | Center 
---------:| :----- |:-----:
Computer  |  $1600 | one
Phone     |    $12 | three
Pipe      |     $1 | eleven